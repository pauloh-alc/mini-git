#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "utilities.h"

typedef struct {
	uint32_t str1_begin, str1_end, str2_begin, str2_end;
} Limits;

Limits get_longest_common_substring(Sliced_str str1, Sliced_str str2) {
	const uint32_t SIZE1 = str1.end - str1.begin + 1,
	               SIZE2 = str2.end - str2.begin + 1;
	char weights[SIZE1][SIZE2];
	uint32_t max_i = 0, max_j = 0;

	for (uint32_t i = 0, k = str1.begin - 1; i < SIZE1; i++, k++) {
		for (uint32_t j = 0, l =  str2.begin - 1; j < SIZE2; j++, l++) {
			if (i == 0 || j == 0) {
				weights[i][j] = 0;
			} else if (str1.chars[k] == str2.chars[l]) {
				weights[i][j] = 1 + weights[i - 1][j - 1];

				if(weights[i][j] > weights[max_i][max_j]) {
					max_i = i;
					max_j = j;
				}
			} else {
				weights[i][j] = 0;
			}
		}
	}

	Limits limits = {
		.str1_begin = max_i - weights[max_i][max_j] + str1.begin,
		.str1_end = max_i + str1.begin,
		.str2_begin = max_j - weights[max_i][max_j] + str2.begin,
		.str2_end = max_j + str2.begin
	};

	return limits;
}

void write_diff_file(Sliced_str str1, Sliced_str str2, FILE *diff) {
	Limits limits = get_longest_common_substring(str1, str2);
	
	if (limits.str1_end == limits.str1_begin && (str1.begin != str1.end || str2.begin != str2.end)) {
		fwrite(&str1.begin, sizeof(uint32_t), 1, diff);

		for (uint32_t i = str2.begin; i < str2.end; i++) {
			fputc(str2.chars[i], diff);
		}

		fputc('\0', diff);
		fwrite(&str1.end, sizeof(uint32_t), 1, diff);
	} else if (limits.str1_begin != limits.str1_end && limits.str2_begin != limits.str2_end) {
		Sliced_str left_str1 = str1;
		Sliced_str left_str2 = str2;

		left_str1.end = limits.str1_begin;
		left_str2.end = limits.str2_begin;
		write_diff_file(left_str1, left_str2, diff);

		Sliced_str right_str1 = str1;
		Sliced_str right_str2 = str2;

		right_str1.begin = limits.str1_end;
		right_str2.begin = limits.str2_end;
		write_diff_file(right_str1, right_str2, diff);
	}
}

char *patch(char *input, FILE *diff) {
	const uint32_t MAX = 8000;
	char *out = malloc(MAX);
	uint32_t idx_input = 0, idx_out = 0;

	while (1) {
		uint32_t insertion_idx;

		fread(&insertion_idx, sizeof(uint32_t), 1, diff);

		if (feof(diff)) {
			break;
		}

		while (idx_input < insertion_idx) {
				out[idx_out] = input[idx_input];
				idx_input++;
				idx_out++;
		}

		while (1) {
				char temp = fgetc(diff);

				if(temp == '\0')
						break;

				out[idx_out] = temp;
				idx_out++;
		}

		fread(&idx_input, sizeof(uint32_t), 1, diff);
	}

	do {
		out[idx_out] = input[idx_input];
		idx_input++;
		idx_out++;
	} while (input[idx_input] != '\0');

	return out;
}
