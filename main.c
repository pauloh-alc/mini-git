#include <stdio.h>
#include <string.h>

#if defined(_WIN32)
	#include <windows.h>

	int make_dir (char *name) {
		return CreateDirectory(name, NULL);
	}
#else
	#include <sys/stat.h>

	int make_dir (char *name) {
		return mkdir(name, 0777);
	}
#endif

int main (int argc, char **argv) {
	if (strcmp(argv[1], "init") == 0) {
		if (argc == 2) {
			make_dir(".mgit");
		} else if (argc == 3) {
			const int SIZE = 50;
			char aux[SIZE];

			make_dir(argv[2]);

			strncpy(aux, argv[2], SIZE - 7);
			strncat(aux, "/.mgit", SIZE);

			make_dir(aux);
		}
	}

	return 0;
}
